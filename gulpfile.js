// TODO: change paths to files
var 
  gulp = require('gulp'),
  sass = require('gulp-sass'),
  mincss = require('gulp-clean-css'),
  rename = require('gulp-rename'),
  path = {
      css: 'src/css/*.css',
      sassBuild: ['src/sass/style.scss'],
      sassWatch: ['src/sass/**/*.scss'],
      img: 'build/img/**/*',
      // pugWatch: 'src/pug/**/*.pug',
      // pugBuild: ['src/pug/*.pug'],
      jsWatch: 'src/js/**/*.js',
      jsBuild: ['src/js/scripts.js']
  },
  plumber = require('gulp-plumber'),
  uglify = require('gulp-uglify'),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  imagemin = require('gulp-imagemin'),
  pug = require('gulp-pug'),
  watch = rename('gulp-watch'),
  csscomb = require('gulp-csscomb'),
  $ = require('gulp-load-plugins')(),
  browserSync = require('browser-sync').create(),
  babel = require('gulp-babel')
;

//SCSS to CSS + prefixer
gulp.task('css', function () {
return gulp.src(path.sassBuild)
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe($.csscomb())
    .pipe(mincss({compatibility: 'ie8'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.stream());
});

// Babel
// gulp.task('babel', function() {
//   return gulp.src('src/js/babel/*.js')
//   .pipe(plumber())
//   .pipe(sourcemaps.init())
//   .pipe(babel({
//       presets: ['env']
//   }))
//   .pipe(concat('scripts.js'))
//   .pipe(sourcemaps.write('/'))
//   .pipe(gulp.dest('src/js'))
// });

//Minify Js
gulp.task('js', function () {
return gulp.src(path.jsBuild)
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('build/js'))
    .pipe(browserSync.stream());
});

//Minify images
gulp.task('imagemin', function () {
return gulp.src(path.img)
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        optimizationLevel: 5,
        interlaced: true
    }))
    .pipe(gulp.dest('build/img'));
});

// Pug compilation
gulp.task('html', function () {
return gulp.src(path.pugBuild)
    .pipe(plumber())
    .pipe(pug({pretty: true}))
    .pipe(gulp.dest('build'))
    .pipe(browserSync.stream());
});

gulp.task('page', ['css', 'js', 'html']);

gulp.task('styles', ['css'])

gulp.task('serve', ['css'], function() {

    browserSync.init({
        server: "./build"
    });

    gulp.watch(path.sassWatch, ['css']);
    // gulp.watch(path.pugWatch, ['html']);
    // gulp.watch(path.jsWatch, ['js']);
    // gulp.watch("build/*.html").on('change', browserSync.reload);
    gulp.watch("build/css/*.css").on('change', browserSync.reload);
    // gulp.watch("build/scripts/*.js").on('change', browserSync.reload);
});

// Gulp watch
gulp.task('watch', function () {
gulp.watch(path.sassWatch, ['css']);
// gulp.watch(path.pugWatch, ['html']);
// gulp.watch(path.jsWatch, ['js']);
});
